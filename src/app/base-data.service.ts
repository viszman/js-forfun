import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class BaseDataService {
  data;

  constructor(private http: Http) { }

  getUsernameData(username: string, callback) {

    this.http
      .get('https://api.github.com/users/' + username).subscribe(res => {
            callback(res.json());
      });
  }
}
