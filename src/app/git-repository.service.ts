import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {forEach} from '@angular/router/src/utils/collection';

@Injectable()
export class GitRepositoryService {

  constructor(private http: Http) { }

  getUserRepositories(username: string, callback) {
      const url = `https://api.github.com/users/${username}/repos`
      this.http
          .get(url).subscribe(res => {
          callback(res.json());
      });
  }

  computeSkills(projects) {
    let keySkills = [];
    const skills = {};
    projects.forEach(node => {
      const language = node.language;
      if (node.language != null) {
          keySkills.push(language);
      }
      if (!skills[language]) {
        skills[language] = 1;
      } else {
        skills[language] ++;
      }
    });
    keySkills = Array.from(new Set(keySkills));
    return {array: keySkills, skills};
  }

  setPercentage(skillNumber, allProjects) {
      const division = skillNumber / allProjects * 100;
      return Math.round(division);;
  }
}
