import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {GitRepositoriesComponent} from '../git-repositories/git-repositories.component';
import {GitUsernameComponent} from '../git-username/git-username.component';
import {StartPageComponent} from '../start-page/start-page.component';
import {ResumeComponent} from '../resume/resume.component';


const routes: Routes = [
    {
        path: '',
        component: StartPageComponent,
    },
    {
        path: 'resume',
        component: ResumeComponent,
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }
