import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GitRepositoriesComponent } from './git-repositories/git-repositories.component';
import { GitUsernameComponent } from './git-username/git-username.component';
import {AppRoutingModule} from './app-routing/app-routing.module';
import { StartPageComponent } from './start-page/start-page.component';
import { ResumeComponent } from './resume/resume.component';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import {BaseDataService} from './base-data.service';
import {GitRepositoryService} from './git-repository.service';

@NgModule({
  declarations: [
    AppComponent,
    GitRepositoriesComponent,
    GitUsernameComponent,
    StartPageComponent,
    ResumeComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule, HttpModule, FormsModule
  ],
  providers: [BaseDataService, GitRepositoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
