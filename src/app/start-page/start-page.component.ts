///<reference path="../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import { Component, OnInit } from '@angular/core';
import {BaseDataService} from '../base-data.service';
import {GitRepositoryService} from '../git-repository.service';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent implements OnInit {
  gitUser;
  repos;
  user;
  languages;

  constructor(
      private userService: BaseDataService,
      private repoService: GitRepositoryService) { }

  ngOnInit() {

  }

  fetchData() {
    this.getBaseInfo();
    this.getRepositories();
  }

  getBaseInfo() {
    this.userService.getUsernameData(this.gitUser, (userData) => {
      this.user = userData;
    });
  }

  getRepositories() {
    this.repoService.getUserRepositories(this.gitUser, (repositories) => {
      this.repos = repositories;
      this.getKeySkills();
    });
  }

  getKeySkills() {
    this.languages = this.repoService.computeSkills(this.repos);
  }

  setPercentage(skillNumber) {
    return this.repoService.setPercentage(skillNumber, this.repos.length);
  }
}
